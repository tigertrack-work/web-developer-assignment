# My Book Collection
My Book Collection is a site that helps you manage your book collection. The main features are:

- Create a new book.
- Read, sort and search from the list of the books.
- Update the book info's.
- Delete a book.
- Export the book list into CSV or XML format.

you can check it out yourself by clicking on this [link](https://aqueous-anchorage-64427.herokuapp.com/).

## Local Setup

1. Before proceeding, make sure you've already installed [Docker](https://docs.docker.com/install) an [Docker](https://docs.docker.com/install) on your system.
2. Clone the repository.
3. `cd` into the repository **root folder** and start the containers by running `docker-compose up -d` in the project root.
4. Install the composer packages by running `docker-compose exec laravel composer install`.
5. You can now access the site locally on `http://localhost` (If there is a "Permission denied" error, run `docker-compose exec laravel chown -R www-data storage`).

`Note:` the changes you make to local files will be automatically reflected in the container. 

### Persistent database
If you want to make sure that the data in the database persists even if the database container is deleted, add a file named `docker-compose.override.yml` in the project root with the following contents.

```
version: "3.7"

services:
  mysql:
    volumes:
    - mysql:/var/lib/mysql

volumes:
  mysql:
```

Then run the following.

```
docker-compose stop \
  && docker-compose rm -f mysql \
  && docker-compose up -d
``` 

## Contributing
We are open for any contributors, you just need to follow these simple rules:

- all PRs must sourced from the development branch.
- please use a concise & informative commit logs. you can refer to this [link](https://www.conventionalcommits.org/en/v1.0.0/) for some samples.
- for major changes, please open an issue first to discuss what you would like to change.
- please make sure to update tests as appropriate.

## Testing
Before running the test, you need to stop the containers by running `docker-compose down`.
After the containers are stopped, open `docker-compose.yml` and change the **APP_ENV** value into `testing`. below are the example
```
...
environment:
      DB_DATABASE: laravel
      DB_HOST: mysql
      DB_PASSWORD: secret
      DB_USERNAME: laravel
      APP_DEBUG: 'true'
      APP_ENV: testing // change  this line
      APP_KEY: ayGbuqVXJakHHvMTZCjKqzhVqMNsHMQs
      LOG_CHANNEL: stderr
...
```
You can now begin testing by running `docker-compose exec laravel vendor/bin/phpunit --testdox`

`Note` that the changes you make to local files will be automatically reflected in the container. 

## Hosting & Deployment
we are using [Heroku](https://heroku.com/) to host this site. below are the steps to deploy:

1. create an heroku app by running `heroku create` command. below are the sample respond:

        $ heroku create
        Creating mighty-hamlet-1982... done, stack is heroku-18
        http://mighty-hamlet-1982.herokuapp.com/ | git@heroku.com:mighty-hamlet-1982.git
        Git remote heroku added
        
2. Use [timanovsky/subdir-heroku-buildpack](https://github.com/timanovsky/subdir-heroku-buildpack) by running `heroku buildpacks:set https://github.com/timanovsky/subdir-heroku-buildpack`.

3. Add heroku default buildpack for php by running `add buildpacks heroku/php`.
4. Tell heroku where your project located, run `heroku config:set PROJECT_PATH=src/`.
5. You need to get your **APP_KEY** from your local setup by running `docker-compose exec laravel php artisan key:generate --show` which will print a key that you can copy and then paste into the next step.
6. Tell heroku your **APP_KEY** by running `heroku config:set APP_KEY=<your-app-key>`. 
7. For database, we recommend using [jawsdb](https://elements.heroku.com/addons/jawsdb). You can use it for your heroku app by running `heroku addons:create jawsdb`. Below are the sample respond:

        $ heroku addons:create jawsdb
        -----> Adding jawsdb to mighty-hamlet-1982... done, v18 (free)
        
8. Once JawsDB has been added to your heroku app, a `JAWSDB_URL` setting will be available in the app configuration and will contain the MySQL connection string. The connection string contains the DB username, password, host and database name that you will need to set on the next step. To obtain this information, you can run `heroku config:get JAWSDB_URL`. Below are the sample respond:

        $ heroku config:get JAWSDB_URL
        mysql://username:password@hostname:port/database_name

9. You can new tell heroku your dabatase configuration by running these command one after another: 

        $ heroku config:set DB_CONNECTION=mysql

        $ heroku config:set DB_HOST=<hostname>

        $ heroku config:set DB_PORT=3306

        $ heroku config:set DB_DATABASE=<database_name>

        $ heroku config:set DB_USERNAME=<username>

        $ heroku config:set DB_PASSWORD=<password>

10. Finally, you can deploy the site by running `git push heroku main`. For deploying using a different branch, you can run `git push heroku <branch_nam>:main` instead.
11. And that should be it! you can now check your deployed site by running `heroku open` or you can go to your heroku dashboard, select your app, and then click **open app** to open it on a new tab.
12. You can run `hero logs --tails` to check on any errors.

  


