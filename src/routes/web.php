<?php

Route::get('/', fn() => view('welcome'));
Route::get('/login', 'Auth\LoginController@form')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware('auth')->group(function () {
  Route::get('/books', 'BookController@index');
  Route::get('/books/create', 'BookController@create');
  Route::post('/books', 'BookController@store');
  Route::get('/books/{book}/edit', 'BookController@edit');
  Route::put('/books/{book}/update', 'BookController@update');
  Route::delete('/books/{book}/delete', 'BookController@destroy');
  Route::get('/export/csv', 'ExportController@exportCsv');
  Route::get('/export/xml', 'ExportController@exportXml');
});