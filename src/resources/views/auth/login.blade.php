@extends('layouts.default')

@section('title')
Login
@endsection

@section('content')
<div class="container">
  
  @if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="row">
      <div class="alert alert-danger">{{ $error }}</div>
    </div>
    @endforeach
  @endif
  
  <div class="row">
    <form method="post" action="/login">
      @csrf
      <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Email address</label>
        <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
      </div>
      <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Password</label>
        <input name="password" type="password" class="form-control" id="exampleInputPassword1">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>
@endsection