@extends('layouts.default')

@section('title')
Add new book
@endsection

@section('content')
<div class="container">
  <div class="row">
  
  </div>
  <div class="row">
    <div class="col">
    <form action="/books" method="post">
      @csrf
      <div class="mb-3">
        <label for="title" class="form-label">Title</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title"
          placeholder="insert book title">
        @error('title')
        <div class="invalid-feedback" role="alert">{{ $message }}</div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="author" class="form-label">Author</label>
        <input type="text" class="form-control @error('author') is-invalid @enderror" name="author" id="author"
          placeholder="insert the name of the author">
        @error('author')
        <div class="invalid-feedback" role="alert">{{ $message }}</div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
  </div>
</div>
@endsection