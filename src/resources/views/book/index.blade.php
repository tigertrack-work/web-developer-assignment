@extends('layouts.default')

@section('title')
Book Collection
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="d-flex justify-content-between">
      <a class="btn btn-primary" href="/books/create" role="button">Add new book</a>
      
      <form method="get" action="">
        <input class="form-control me-2" type="search" name="keyword" placeholder="Search" aria-label="Search">
      </form>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Title 
                @if(isset($_GET['direction']) && $_GET['direction'] == 'asc')
                <a href="?orderBy=title&direction=desc{{(isset($_GET['keyword'])) ? '&keyword='.$_GET['keyword']: ''}}" class="link-dark"><i class="bi-sort-down"></i></a>
                @else
                <a href="?orderBy=title&direction=asc{{(isset($_GET['keyword'])) ? '&keyword='.$_GET['keyword']: ''}}" class="link-dark"><i class="bi-sort-up"></i></a>
                @endif
              </th>
              <th>Author
                 @if(isset($_GET['direction']) && $_GET['direction'] == 'asc')
                <a href="?orderBy=author&direction=desc" class="link-dark"><i class="bi-sort-down"></i></a>
                @else
                <a href="?orderBy=author&direction=asc" class="link-dark"><i class="bi-sort-up"></i></a>
                @endif
              </th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($books as $book)
            <tr id="book-{{$book->id}}">
              <td>{{ $book->title }}</td>
              <td>{{ $book->author }}</td>
              <td>
              <a href="/books/{{ $book->id }}/edit" class="btn btn-primary" role="button" ><i class="bi-pencil"></i></a>
                <a class="btn btn-danger" role="button" onClick="confirmDelete({{ $book->id }})"><i class="bi-trash"></i></a>

              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      {{ $books->appends($_GET)->links('vendor.pagination.simple-bootstrap-4', ['filters' => $filters]) }}
    </div>
  </div>
</div>
@endsection

@section('js-content')
<script>
  function confirmDelete(id){
    Swal.fire({
      title: 'Delete Confirmation',
      text: "Are you sure? You won't be able to revert this!",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/books/${id}/delete`)
        .then(response => {
          if(response.status == 204){
            document.querySelector(`tr#book-${id}`).remove();
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
          }
          else{
            Swal.fire(
              'Oopss, something wrong!',
              `The server responded with code ${response.status}`,
              'error'
            )
          }
        })
      }
    })
  }
</script>
@endsection