
<nav class="d-flex justify-content-between">
    @if ($paginator->hasPages())
    <ul class="pagination mb-0" >
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled" aria-disabled="true">
                <span class="page-link">@lang('pagination.previous')</span>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a>
            </li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a>
            </li>
        @else
            <li class="page-item disabled" aria-disabled="true">
                <span class="page-link">@lang('pagination.next')</span>
            </li>
        @endif
    </ul>
    @endif
    <div class="btn-group" role="group" aria-label="Basic example">
        <button type="button" class="btn btn-outline-secondary px-4"
            id="dropDownCsv" data-bs-toggle="dropdown" aria-expanded="false">
            <i class="bi-download pe-2"></i>CSV
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropDownCsv">
            <li><h6 class="dropdown-header">Export Current</h6></li>
            <li><a class="dropdown-item" href="/export/csv?{{$filters}}">Title & Author</a></li>
            <li><a class="dropdown-item" href="/export/csv?TitleOnly=true&{{$filters}}">Title Only</a></li>
            <li><a class="dropdown-item" href="/export/csv?AuthorOnly=true&{{$filters}}">Author Only</a></li>
            <li><h6 class="dropdown-header">Export All</h6></li> 
            <li><a class="dropdown-item" href="/export/csv?exportAll=true&{{$filters}}">Title & Author</a></li> 
            <li><a class="dropdown-item" href="/export/csv?exportAll=true&TitleOnly=true&{{$filters}}">Title Only</a></li> 
            <li><a class="dropdown-item" href="/export/csv?exportAll=true&AuthorOnly=true&{{$filters}}">Author Only</a></li> 
        </ul>
        <button type="button" class="btn btn-outline-secondary px-4"
            id="dropDownXml" data-bs-toggle="dropdown" aria-expanded="false">
            <i class="bi-download pe-2"></i>XML
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropDownXml">
            <li><h6 class="dropdown-header">Export Current</h6></li>
            <li><a class="dropdown-item" href="/export/xml?{{$filters}}">Title & Author</a></li>
            <li><a class="dropdown-item" href="/export/xml?TitleOnly=true&{{$filters}}">Title Only</a></li>
            <li><a class="dropdown-item" href="/export/xml?AuthorOnly=true&{{$filters}}">Author Only</a></li>
            <li><h6 class="dropdown-header">Export All</h6></li> 
            <li><a class="dropdown-item" href="/export/xml?exportAll=true&{{$filters}}">Title & Author</a></li> 
            <li><a class="dropdown-item" href="/export/xml?exportAll=true&TitleOnly=true&{{$filters}}">Title Only</a></li> 
            <li><a class="dropdown-item" href="/export/xml?exportAll=true&AuthorOnly=true&{{$filters}}">Author Only</a></li> 
        </ul>
    </div>
</nav>

