@extends('layouts.default')

@section('title')
Landing
@endsection

@section('content')
<main class="px-3">
    <h1>Hi There!</h1>
    <p class="lead">Welcome to My Book Collection, where you can create, read, update and delete (softly, of course!) our shared book collection.<br>
    In this site, you can:
    <ul class="list-unstyled">
        <li><strong>Create</strong> a new book.</li>
        <li><strong>Read, sort and search</strong> from the list of the books.</li>
        <li><strong>Update</strong> the book info's.</li>
        <li><strong>Delete</strong> a book.</li>
        <li>And <strong>Export</strong> the book list into <strong>CSV</strong> or <strong>XML</strong> format.</li>

    </ul>
    Please click the login button on the top right corner to start your experience.</p>
    <p class="lead">
      <a href="#" class="btn btn-lg btn-secondary fw-bold border-white bg-white">Learn more</a>
    </p>
  </main>
@endsection

