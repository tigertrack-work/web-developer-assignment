<?php
namespace App\Services;

class Export {
  public static function xmlBuilder($data, $filename, $rootName = 'document', $itemName = 'item'){
        
    $str = '';
    foreach ($data as $item) {
        $str .= "<$itemName>";
        foreach ($item as $key => $value) {
            $str .= "<$key>$value</$key>";
        }
        $str .= "</$itemName>";
    }

    $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><'.$rootName.'>'.$str.'</'.$rootName.'>');

    $xml->addAttribute('export_date', date('Y-m-d'));

    return $xml;
  }

  public static function csvBuilder($data){
    $headers = array_keys($data[0]);
    
    $callback = function() use($data, $headers) {
        $file = fopen('php://output', 'w');
        fputcsv($file, $headers);

        foreach ($data as $item) {
            $rows = array();
            foreach ($item as $key => $value) {
                array_push($rows, $value);
            }
            fputcsv($file, $rows);
        }

        fclose($file);
    };

    return $callback;
  }
}
