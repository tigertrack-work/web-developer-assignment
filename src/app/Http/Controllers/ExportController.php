<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Services\Export;

class ExportController extends Controller
{
    public function exportCsv(Request $request){
        $select = ($request->get('TitleOnly')) ? ['title'] : (($request->get('AuthorOnly')) ? ['author'] : ['title', 'author']);
        $orderBy = $request->get('orderBy');
        $direction = $request->get('direction');
        $keyword = $request->get('keyword') ?? '';
        $all = $request->get('exportAll') ?? false;

        if(!empty($orderBy) && !empty($direction))
            $books = Book::select($select)
                ->orderBy($orderBy, $direction)
                ->where('title', 'LIKE', '%'.$keyword.'%')
                ->orWhere('author', 'LIKE', '%'.$keyword.'%');
            
        else
            $books = Book::select($select)
                ->where('title', 'LIKE', '%'.$keyword.'%')
                ->orWhere('author', 'LIKE', '%'.$keyword.'%');
                
        if($all){
            $books = $books->get();
        }else{
            $books = $books->simplePaginate(10);
        }
        $fileName = 'books.csv';

        if($request->input('TitleOnly'))
            $fileName = 'books_title_only.csv';
            
        else if($request->input('AuthorOnly'))
            $fileName = 'books_author_only.csv';

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        return response()->stream(Export::csvBuilder(
            ($all) ? $books->toArray() : $books->getCollection()->toArray()
            ), 200, $headers);
    }

    public function exportXml(Request $request){

        $select = ($request->get('TitleOnly')) ? ['title'] : (($request->get('AuthorOnly')) ? ['author'] : ['title', 'author']);
        $orderBy = $request->get('orderBy');
        $direction = $request->get('direction');
        $keyword = $request->get('keyword') ?? '';
        $all = $request->get('exportAll') ?? false;

        if(!empty($orderBy) && !empty($direction))
            $books = Book::select($select)
                ->orderBy($orderBy, $direction)
                ->where('title', 'LIKE', '%'.$keyword.'%')
                ->orWhere('author', 'LIKE', '%'.$keyword.'%');
            
        else
            $books = Book::select($select)
                ->where('title', 'LIKE', '%'.$keyword.'%')
                ->orWhere('author', 'LIKE', '%'.$keyword.'%');
                
        if($all){
            $books = $books->get();
        }else{
            $books = $books->simplePaginate(10);
        }
        
        $fileName = 'books.xml';

        if($request->input('TitleOnly'))
            $fileName = 'books_title_only.xml';
            
        else if($request->input('AuthorOnly'))
            $fileName = 'books_author_only.xml';
        
        $export = Export::xmlBuilder(($all) ? $books->toArray() : $books->getCollection()->toArray(), 
            $fileName, 'books', 'book');

        $response = \Response::make($export->asXML(), 200);
        $response->header('Content-Type', 'text/xml');
        $response->header('Cache-Control', 'public');
        $response->header('Content-Description', 'File Transfer');
        $response->header('Content-Disposition', 'attachment; filename='.$fileName);
        $response->header('Content-Transfer-Encoding', 'binary');

        return $response;
        
    }
}
