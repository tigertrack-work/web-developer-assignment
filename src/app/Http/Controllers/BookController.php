<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;
use App\Services\Export;

class BookController extends Controller
{
    public function index(Request $request)
    {
        $orderBy = $request->get('orderBy');
        $direction = $request->get('direction');
        $keyword = $request->get('keyword') ?? '';

        $filters = http_build_query($request->only('orderBy', 'direction', 'keyword', 'page'));

        if(!empty($orderBy) && !empty($direction))
            $books = Book::orderBy($orderBy, $direction)
            ->where('title', 'LIKE', '%'.$keyword.'%')
            ->orWhere('author', 'LIKE', '%'.$keyword.'%')
            ->simplePaginate(10);
        else
            $books = Book::where('title', 'LIKE', '%'.$keyword.'%')
                ->orWhere('author', 'LIKE', '%'.$keyword.'%')
                ->simplePaginate(10);

        return view('book.index')->with('books', $books)->with('filters', $filters);
    }

    public function create()
    {
        return view('book.create');
    }

    public function store(BookRequest $request)
    {
        Book::create([
            'created_by' => auth()->user()->id,
            'title' => $request->title,
            'author' => $request->author
        ]);

        return redirect('/books');
    }

    public function edit(Book $book)
    {
        return view('book.edit')->with('book', $book);
    }

    public function update(BookRequest $request, Book $book)
    {
        $book->title = $request->title;
        $book->author = $request->author;
        $book->updated_by = auth()->user()->id;
        $book->save();

        return redirect('/books');
    }

    public function destroy(Book $book)
    {
        $book->deleted_by = auth()->user()->id;
        $book->save();
        $book->delete();

        return response(null, 204);
    }
}
