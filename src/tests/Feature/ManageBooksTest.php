<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Book;
use App\User;

class ManageBooksTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function can_create_a_book()
    {
        $user = factory(User::class)->create();
        $book = factory('App\Book')->make(['created_by' => $user->id]);
        $this->actingAs($user)->call('POST', '/books',$book->toArray());
        $this->assertEquals(1,Book::all()->count());
    }

    /** @test */
    public function can_show_book_collection()
    {
        $user = factory(User::class)->create();
        $books = factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->call('GET', '/books');
        $response->assertSeeInOrder($books->pluck('title')->all())
            ->assertSeeInOrder($books->pluck('author')->all());
    }

    /** @test */
    public function can_sort_by_title_asc()
    {
        $user = factory(User::class)->create();
        factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->call('GET', '/books?', ['orderBy' => 'title', 'direction' => 'asc']);
        $asc_sorted = Book::orderBy('title', 'asc')->get();
        $response->assertSeeInOrder($asc_sorted->pluck('title')->all());
    }

    /** @test */
    public function can_sort_by_title_desc()
    {
        $user = factory(User::class)->create();
        factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->call('GET', '/books?', ['orderBy' => 'title', 'direction' => 'desc']);
        $desc_sorted = Book::orderBy('title', 'desc')->get();
        $response->assertSeeInOrder($desc_sorted->pluck('title')->all());
    }

    /** @test */
    public function can_sort_by_author_asc()
    {
        $user = factory(User::class)->create();
        factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->call('GET', '/books?', ['orderBy' => 'author', 'direction' => 'asc']);
        $asc_sorted = Book::orderBy('author', 'asc')->get();
        $response->assertSeeInOrder($asc_sorted->pluck('author')->all());
    }

    /** @test */
    public function can_sort_by_author_desc(){
        $user = factory(User::class)->create();
        factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->call('GET', '/books?', ['orderBy' => 'author', 'direction' => 'desc']);
        $desc_sorted = Book::orderBy('author', 'desc')->get();
        $response->assertSeeInOrder($desc_sorted->pluck('author')->all());
    }

    /** @test */
    public function can_search_book_by_author()
    {
        $user = factory(User::class)->create();
        $book1 = factory('App\Book')->create(['created_by' => $user->id]);
        $book2 = factory('App\Book')->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->call('GET', '/books?', ['keyword' => $book1->author]);
        $response->assertSee($book1->author)
            ->assertDontSee($book2->author);
    }

    /** @test */
    public function can_search_book_by_title()
    {
        $user = factory(User::class)->create();
        $book1 = factory('App\Book')->create(['created_by' => $user->id]);
        $book2 = factory('App\Book')->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->call('GET', '/books?', ['keyword' => $book1->title]);
        $response->assertSee($book1->title)
            ->assertDontSee($book2->title);
    }

    /** @test */
    public function can_update_author()
    {
        $user = factory(User::class)->create();
        $this->withoutExceptionHandling();
        $book = factory('App\Book')->create(['created_by' => $user->id]);
        $new_author = "new author";
        $response = $this->actingAs($user)->call('PUT', "books/$book->id/update", 
            [
                '_token' => csrf_token(),
                'author' => $new_author,
                'title' => $book->title
            ]);
        $this->assertEquals($new_author, Book::find($book->id)->author);
    }

    /** @test */
    public function can_update_title()
    {
        $user = factory(User::class)->create();
        $this->withoutExceptionHandling();
        $book = factory('App\Book')->create(['created_by' => $user->id]);
        $new_title = "new title";
        $response = $this->actingAs($user)->call('PUT', "books/$book->id/update", 
            [
                '_token' => csrf_token(),
                'title' => $new_title,
                'author' => $book->author
            ]);
        $this->assertEquals($new_title, Book::find($book->id)->title);
    }

    /** @test */
    public function can_delete_a_book()
    {
        $user = factory(User::class)->create();
        $book = factory('App\Book')->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->call('DELETE', "books/$book->id/delete");
        $this->assertNull(Book::find($book->id));
    }
}
