<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class ExportTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_export_csv()
    {
        $user = factory(User::class)->create();
        $books = factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->get('/export/csv');
        $response->assertHeader('Content-Disposition', 'attachment; filename=books.csv');
        
    }

    /** @test */
    public function can_export_csv_author_only()
    {
        $user = factory(User::class)->create();
        $books = factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->get('/export/csv?AuthorOnly=true');
        $response->assertHeader('Content-Disposition', 'attachment; filename=books_author_only.csv');
    }

    /** @test */
    public function can_export_csv_title_only()
    {
        $user = factory(User::class)->create();
        $books = factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->get('/export/csv?TitleOnly=true');
        $response->assertHeader('Content-Disposition', 'attachment; filename=books_title_only.csv');
    }

    /** @test */
    public function can_export_xml()
    {
        $user = factory(User::class)->create();
        $books = factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->get('/export/xml');
        $response->assertHeader('Content-Disposition', 'attachment; filename=books.xml');
    }

    /** @test */
    public function can_export_xml_author_only()
    {
        $user = factory(User::class)->create();
        $books = factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->get('/export/xml?AuthorOnly=true');
        $response->assertHeader('Content-Disposition', 'attachment; filename=books_author_only.xml');
    }

    /** @test */
    public function can_export_xml_title_only()
    {
        $user = factory(User::class)->create();
        $books = factory('App\Book', 5)->create(['created_by' => $user->id]);
        $response = $this->actingAs($user)->get('/export/xml?TitleOnly=true');
        $response->assertHeader('Content-Disposition', 'attachment; filename=books_title_only.xml');
    }

}
