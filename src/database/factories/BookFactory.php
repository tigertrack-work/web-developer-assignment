<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        /* single quote converted into &#039; in view, causing assertSeeInOrder to fail */
        'author' => str_replace("'", " ", $faker->name($gender = 'male')) 
    ];
});
