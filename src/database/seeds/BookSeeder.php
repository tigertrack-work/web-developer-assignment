<?php

use Illuminate\Database\Seeder;
use App\Book;
use App\User;
class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
        'name' => 'admin'],[
            'email' => 'admin@app.com',
            'password' => Hash::make('admin')
        ])->each(function ($user) {
            $user->books()->createMany(factory(App\Book::class, 50)->make()->toArray());
        });
        
    }
}
